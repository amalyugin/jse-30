package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M extends AbstractModel> {

        @NotNull
        Optional<M> findOneById(@NotNull String id);

        @NotNull
        Optional<M> findOneByIndex(@NotNull Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {

            @Override
            @NotNull
            public Optional<M> findOneById(@NotNull String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            @NotNull
            public Optional<M> findOneByIndex(@NotNull Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }

        };
    }

    void clear();

    int getSize();

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@NotNull List<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

}