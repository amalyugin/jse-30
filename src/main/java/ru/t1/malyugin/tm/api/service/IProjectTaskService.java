package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Project removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}