package ru.t1.malyugin.tm.exception.user;

public final class LoginExistException extends AbstractUserException {

    public LoginExistException() {
        super("Error! Login already exists...");
    }

}