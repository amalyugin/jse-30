package ru.t1.malyugin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FileScanner extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final String commandDir;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.commandDir = bootstrap.getPropertyService().getApplicationCommandDir();
        this.setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommands();
        commands.forEach(command -> this.commands.add(command.getName()));
        this.start();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);

            @NotNull final Set<File> files = Stream.of(new File(commandDir).listFiles())
                    .filter(f -> !f.isDirectory())
                    .collect(Collectors.toSet());

            for (@NotNull final File file : files) {
                @NotNull final String fileName = file.getName();
                final boolean check = commands.contains(fileName);
                try {
                    if (check) {
                        bootstrap.processCommand(fileName, true);
                        Files.deleteIfExists(Paths.get(commandDir + fileName));
                    }
                } catch (@NotNull final Exception e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }

}